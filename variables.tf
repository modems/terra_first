variable "project" {
  description = "ansible-322522"
  default = "ansible-322522"
}
variable "region" {
  description = "europe-west-1"
  default = "europe-west1"
}
variable "zone" {
  description = "europe-west1-b"
  default     = "europe-west1-b"
}
variable "instances_disk_image" {
  description = "Disk image"
  default     = "debian-cloud/debian-10"
}
variable "node_count" {
  description = "number of nodes"
  default     = "3"
}