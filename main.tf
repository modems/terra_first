resource "google_compute_instance" "nodes-228" {
  count         = var.node_count
  name          = "node${count.index + 1}"
  machine_type = "e2-small"
  zone         = var.zone
  tags         = ["nodes-228"]
  boot_disk {
    initialize_params { 
        image = var.instances_disk_image
        size = 20
    }
  }
  network_interface {
    network = "default"
  }
}